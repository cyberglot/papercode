{-# LANGUAGE TypeOperators #-}

module Exp where

data Exp f = In (f (Exp f))

data Val e = Val Int
type IntExp = Exp Val

data Add e = Add e e
type AddExp = Exp Add

data (f :+: g) e = Inl (f e) | Inr (g e)

addExample :: Exp (Val :+: Add)
addExample = In (Inr (Add (In (Inl (Val 118))) (In (Inl (Val 1219))))) -- val 118 + val 1219

instance Functor Val where
  fmap _ (Val x) = Val x

instance Functor Add where
  fmap f (Add x y) = Add (f x) (f y)

instance (Functor f, Functor g) => Functor (f :+: g) where
  fmap f (Inl e) = Inl (fmap f e)
  fmap f (Inr e) = Inr (fmap f e)

fold :: Functor f => (f a -> a) -> Exp f -> a
fold g (In t) = g $ fmap (fold g) t

class Functor f => Eval f where
  evalA :: f Int -> Int

instance Eval Val where
  evalA (Val x) = x

instance Eval Add where
  evalA (Add x y) = x + y

instance (Eval f, Eval g) => Eval (f :+: g) where
  evalA (Inl x) = evalA x
  evalA (Inr x) = evalA x

eval :: Eval f => Exp f -> Int
eval = fold evalA
