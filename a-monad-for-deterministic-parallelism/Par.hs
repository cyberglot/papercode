{-# LANGUAGE ExistentialQuantification
           , BangPatterns
           , NamedFieldPuns             #-}

module Main where

import Control.DeepSeq
import Control.Monad
import Data.IORef
import System.IO.Unsafe
import Control.Concurrent
import GHC.Conc (numCapabilities)
import Data.Char (chr)

data Trace = Fork Trace Trace
           | Done
           | forall a . Get (IVar a) (a -> Trace)
           | forall a . Put (IVar a) a Trace
           | forall a . New (IVar a -> Trace)

newtype Par a = Par { unPar :: (a -> Trace) -> Trace }

newtype IVar a = IVar (IORef (IVarContents a))

data IVarContents a = Empty | Full a | Blocked [a -> Trace]

instance Eq (IVar a) where
  IVar a == IVar b = a == b

instance NFData (IVar a) where
  rnf _ = ()

fork :: Par () -> Par ()
fork p = Par $
  \c -> Fork (unPar p (\_ -> Done)) (c ())

new :: Par (IVar a)
new = Par $ New

get :: IVar a -> Par a
get v = Par $ \c -> Get v c

put :: NFData a => IVar a -> a -> Par ()
put v a = deepseq a (Par $ \c -> Put v a (c ()))

put_ :: IVar a -> a -> Par ()
put_ v !a = Par $ \c -> Put v a (c ())

instance Functor Par where
  fmap f a = Par $ \c -> unPar a (c . f)

instance Applicative Par where
  pure = return
  (<*>) = ap

instance Monad Par where
  return a = Par $ \c -> c a
  m >>= k  = Par $
    \c -> unPar m (\a -> unPar (k a) c)

-- sequential scheduler

type SchedState' = [Trace]

reschedule' :: SchedState' -> IO ()
reschedule' []     = return ()
reschedule' (t:ts) = sched' ts t

sched' :: SchedState' -> Trace -> IO ()
sched' state (Fork child parent) = sched' (child:state) parent
sched' state Done                = reschedule' state
sched' state (New f)             = newIORef (Blocked []) >>= \r -> sched' state . f $ IVar r
sched' state (Get (IVar v) c)    = readIORef v >>= \e -> case e of
                                                          Empty      -> writeIORef v (Blocked [c]) >> reschedule' state
                                                          Full    a  -> sched' state (c a)
                                                          Blocked cs -> writeIORef v (Blocked (c:cs)) >> reschedule' state
sched' state (Put (IVar v) a t)  = do
  cs <- atomicModifyIORef v $ \e -> case e of
                                Empty      -> (Full a, [])
                                Full _     -> error "multiple put"
                                Blocked cs -> (Full a, cs)
  let st = map ($ a) cs ++ state
  sched' st t

runPar' :: Par a -> a
runPar' x = unsafePerformIO $ do
  rref <- newIORef (Empty)
  sched' [] $ unPar (x >>= put_ (IVar rref)) (const Done)
  r <- readIORef rref
  case r of
    Full a -> return a
    _      -> error "no result"

-- work-stealing scheduler

data SchedState = SchedState
  { no       :: Int
  , workpool :: IORef [Trace]
  , idle     :: IORef [MVar Bool]
  , scheds   :: [SchedState]      }

reschedule :: SchedState -> IO ()
reschedule state@SchedState{ workpool } =
  (atomicModifyIORef workpool $ \ts ->
      case ts of
        []      -> ([], Nothing)
        (t:ts') -> (ts', Just t)) >>= \e -> case e of
                                              Nothing -> steal state
                                              Just t  -> sched state t


pushWork :: SchedState -> Trace -> IO ()
pushWork SchedState { workpool, idle } t = do
  atomicModifyIORef workpool $ \ts -> (t:ts, ())
  idles <- readIORef idle
  when (not (null idles)) $ do
       r <- atomicModifyIORef idle
         $ \is -> case is of
                    []     -> ([], return ())
                    (i:is) -> (is, putMVar i False)
       r

sched :: SchedState -> Trace -> IO ()
sched state (Fork child parent) = pushWork state child >> sched state parent
sched state Done                = reschedule state
sched state (New f)             =
  newIORef (Blocked []) >>= \r -> sched state . f $ IVar r
sched state (Get (IVar v) c)    =
  readIORef v >>= \e -> case e of
                          Full a -> sched state (c a)
                          _      -> (atomicModifyIORef v $ \e ->
                                        case e of
                                          Empty      -> (Blocked [], reschedule state)
                                          Full    a  -> (Full a, sched state (c a))
                                          Blocked cs -> (Blocked (c:cs), reschedule state)
                                    ) >>= id
sched state (Put (IVar v) a t) =
  (atomicModifyIORef v $ \e ->
      case e of
        Empty      -> (Full a, [])
        Full    _  -> error "multiple put"
        Blocked cs -> (Full a, cs)
  )
  >>= \cs -> mapM_ (pushWork state . ($ a)) cs >> sched state t

steal :: SchedState -> IO ()
steal state@SchedState{ idle, scheds, no = my_no } = go scheds
  where
    go (x:xs)
      | no x == my_no = go xs
      | otherwise     = do
          r <- atomicModifyIORef (workpool x) $
            \ts -> case ts of
              []     -> ([], Nothing)
              (x:xs) -> (xs, Just x)
          case r of
            Just t  -> sched state t
            Nothing -> go xs
    go [] = do
      m <- newEmptyMVar
      r <- atomicModifyIORef idle $ \is -> (m:is, is)
      if length r == numCapabilities - 1
        then mapM_ (\m -> putMVar m True) r
        else do done <- takeMVar m
                if done then return ()
                  else go scheds


runPar :: Par a -> a
runPar x = unsafePerformIO $ do
  let n = numCapabilities
  workpools <- replicateM n $ newIORef []
  is <- newIORef []
  let states = [ SchedState { no = y
                            , workpool = wp
                            , idle = is
                            , scheds = states }
               | (y, wp) <- zip [0..] workpools ]
  m <- newEmptyMVar
  (main, _) <- threadCapability =<< myThreadId
  forM_ (zip [0..] states) $ \(cpu, state) ->
    forkOn cpu $
      if (cpu /= main)
      then reschedule state
      else do
        rref <- newIORef Empty
        sched state $
          unPar (x >>= put_ (IVar rref)) (const Done)
        readIORef rref >>= putMVar m
  r <- takeMVar m
  case r of
    Full a -> return a
    _      -> error "no result"

spawn :: NFData a => Par a -> Par (IVar a)
spawn p = do
  i <- new
  fork (do x <- p
           put i x
       )
  return i

parMapM :: NFData b => (a -> Par b) -> [a] -> Par [b]
parMapM f as = do
  ibs <- mapM (spawn . f) as
  mapM get ibs

divConq :: NFData a
        => (prob -> Bool)
        -> (prob -> [prob])
        -> ([a] -> a)
        -> (prob -> a)
        -> (prob -> a)
divConq indiv split join f prob =
  runPar $ go prob
  where
    go prob
      | indiv prob = return (f prob)
      | otherwise  = do
          sols <- parMapM go (split prob)
          return (join sols)

main :: IO ()
main = putStrLn . runPar $ parMapM (return . chr) ([33..126] :: [Int])
