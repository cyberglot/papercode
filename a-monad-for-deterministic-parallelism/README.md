# A monad for deterministic parallelism

[[Paper](https://www.microsoft.com/en-us/research/wp-content/uploads/2011/01/monad-par.pdf)]

The code implemented here differs a bit from the paper. I decided to add `Empty` to the `IVar a`
datatype, and all the code has been changed to accommodate that. Also, the `runPar` function 
for the work-stealing scheduler fixes the errors in the paper, mainly by fixing confusing var naming
(`states`|`scheds` problem), adding the `main_cpu` var and using `Empty` correctly (without `Empty`, it 
should've been `Blocked []` instead).

## Running

```bash
$ stack build
$ stack exec par
```
