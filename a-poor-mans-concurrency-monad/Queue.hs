module Queue where

import Concurrency
import Writer
import MVar
import Control.Monad
import Control.Monad.Trans
import System.IO
import Data.IORef

bloop :: IORef Int -> Con IO ()
bloop v = lift (readIORef v >>=
                \x -> writeIORef v (x + 1) >> return (x > 100000))
  >>= \b -> if b then return () else bloop v

crazy :: Int -> Con IO ()
crazy n = do v <- lift (newIORef (0 :: Int))
             sequence_ $
               foldr (\ _ -> (fork (bloop v) :))
                 [last v]
                 [1 .. n] where
     last :: IORef Int -> Con IO ()
     last v = do bloop v
                 x <- lift (readIORef v)
                 lift (putStrLn (show x))

data Queue a = Queue [a] [a]

enqueue :: a -> Queue a -> Queue a
enqueue e (Queue front back) = Queue front (e : back)

dequeue :: Queue a -> Maybe (a, Queue a)
dequeue (Queue (e : front) back) = Just (e, Queue front back)
dequeue (Queue []          []  ) = Nothing
dequeue (Queue []          back) = dequeue (Queue (reverse back) [])

qsched :: Monad m => Queue (Action m) -> m ()
qsched q = case dequeue q of
  Nothing -> return ()
  Just (a, as) -> case a of
     Atom am    -> am >>= \a' -> qsched (enqueue a' as)
     Fork a1 a2 -> qsched (enqueue a2 (enqueue a1 as))
     Stop       -> qsched as

single :: a -> Queue a
single x = (Queue [x] [])

qrun :: Monad m => Con m a -> m ()
qrun m = qsched (single (action m))

main :: IO ()
main = run (crazy 1000) >> qrun (crazy 1000)
