module Concurrency where

import Control.Monad
import Control.Monad.Trans
import System.IO
import Writer

data Action m
  = Atom (m (Action m))
  | Fork (Action m) (Action m)
  | Stop

newtype Con m a =
  Con { apply :: (a -> Action m) -> Action m }

instance Monad m => Functor (Con m) where
  fmap f (Con ma) = Con $ \c -> ma (c . f)

instance Monad m => Monad (Con m) where
  return a = Con $ \c -> c a

  m >>= f = Con $ \k -> apply m $ \a -> apply (f a) k

instance Monad m => Applicative (Con m) where
  pure = return
  (<*>) = ap

atom :: Monad m => m a -> Con m a
atom m = Con $ \k -> Atom $ m >>= \a -> return (k a)

action :: Monad m => Con m a -> Action m
action m = apply m $ \_ -> Stop

stop :: Monad m => Con m a
stop = Con $ \_ -> Stop

par :: Monad m => Con m a -> Con m a -> Con m a
par m n = Con $ \c -> Fork (apply m c) (apply n c)

fork :: Monad m => Con m a -> Con m ()
fork m = Con $ \c -> Fork (action m) (c ())

instance MonadTrans Con where
  lift = atom

round :: Monad m => [Action m] -> m ()
round []       = return ()
round (a : as) = case a of
  Atom am  -> am >>= \a' -> Concurrency.round (as ++ [a'])
  Fork b c -> Concurrency.round (as ++ [b, c])
  Stop     -> Concurrency.round as

run :: Monad m => Con m a -> m ()
run m = Concurrency.round [ action m ]

instance Writer m => Writer (Con m) where
  write []      = return ()
  write (c : s) = lift (write [c]) >> write s

loop :: Writer m => String -> m ()
loop s = write s >> loop s

example :: Writer m => Con m ()
example = write "start!" >> fork (loop "fish") >> loop "cat"

class Monad m => Input m where
  input :: m (Maybe String)

instance Input IO where
  input = hReady stdin >>=
    \x -> if x then liftM Just getLine else return Nothing

ioloop :: (Input m, Writer m) => String -> m String
ioloop s = input >>=
  \i -> case i of
          Just x  -> return ("Thread " ++ s ++ ":" ++ x)
          Nothing -> write s >> ioloop s

instance Input m => Input (Con m) where
  input = lift input

example2 :: (Input m, Writer m) => Con m ()
example2 = fork (ioloop "a" >>= write) >> fork (ioloop "b" >>= write)

example3 :: (Input m, Writer m) => Con m ()
example3 = par (ioloop "a") (ioloop "b") >>= write
