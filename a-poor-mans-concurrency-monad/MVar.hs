module MVar where

import Concurrency
import Writer
import Data.Monoid
import Control.Monad
import Control.Monad.Writer
import Control.Monad.Trans
import System.IO
import Data.IORef

type MVar a = IORef (Maybe a)

class Monad m => MMVar m where
  newMVar :: m (MVar a)
  putMVar :: MVar a -> a -> m ()
  getMVar :: MVar a -> m (Maybe a)

instance MMVar IO where
  newMVar     = newIORef Nothing
  putMVar v a = writeIORef v (Just a)
  getMVar v   = readIORef v >>= \a -> writeIORef v Nothing >> return a

instance MMVar m => MMVar (Con m) where
  newMVar     = lift newMVar
  putMVar v a = lift $ putMVar v a
  getMVar v   = lift $ getMVar v

get :: (MMVar m) => MVar a -> m a
get v = getMVar v >>= \mv -> case mv of
                               Just a  -> return a
                               Nothing -> get v

data Msg
  = Add
  | Reset
  | Print
  | Quit

simulation :: MVar Msg -> Integer -> Con IO ()
simulation mv i = getMVar mv >>=
  \x -> case x of
          Just Add   -> write "add\n" >> simulation mv (i + 1)
          Just Reset -> write "reset\n" >> simulation mv 0
          Just Print -> write ("print " ++ show i ++ "\n") >> simulation mv i
          Just Quit  -> write "done\n"
          Nothing    -> simulation mv i

interface :: MVar Msg -> Con IO (Maybe String) -> Con IO ()
interface mv getInput = loop where
  loop = getInput >>= \k ->
    case k of
      Just "a" -> putMVar mv Add   >> loop
      Just "r" -> putMVar mv Reset >> loop
      Just "p" -> putMVar mv Print >> loop
      Just "q" -> putMVar mv Quit
      Just s   -> write ("Unknown command: " ++ s) >> loop
      Nothing  -> loop

example :: Con IO ()
example = newMVar >>= \mv -> fork (simulation mv 0) >> (interface mv input)
