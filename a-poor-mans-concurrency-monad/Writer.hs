module Writer where

import Data.Tuple (swap)

newtype W a = W { runWriter :: (a, String) }

mapFst :: (a -> b) -> (a, c) -> (b, c)
mapFst f = swap . fmap f . swap

instance Functor W where
  fmap f (W sas) = W $ mapFst f sas

instance Applicative W where
  pure a = W (a, "")

  W (f, s) <*> W (a, s') = W (f a, mappend s s')

instance Monad W where
  return = pure

  (W (a, s)) >>= f = let (a', s') = runWriter (f a)
                     in W (a', mappend s s')

class Monad m => Writer m where
  write :: String -> m ()

instance Writer W where
  write s = W ((), s)

instance Writer IO where
  write = putStr

output :: W a -> String
output wa = snd . runWriter $ wa
