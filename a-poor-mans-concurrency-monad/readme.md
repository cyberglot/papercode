# A poor man's concurrency monad

[Paper](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.8039)

I've also implemented some of the stuff presented [here](http://www.seas.upenn.edu/~cis552/11fa/lectures/concurrency.html).
